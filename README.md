# PDF

A PDF converter.
https://pdf.nexylan.dev

## Usage

```
$ curl --request POST \
  --url https://pdf.nexylan.dev/convert/html \
  --header 'Content-Type: multipart/form-data' \
  --form files=@index.html \
  --output result.pdf
```

The Gotemberg API provide multiple endpoints to convert html, markdown and even office documents!

This project also provides some dev clients.

All are well documented on the [Gotenberg project website](https://gotenberg.dev).

## Development

To run the project locally:

```
make
```

With debug log:

```
LOG_LEVEL=DEBUG make
```
